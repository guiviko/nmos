import {ActionReducer, Action} from '@ngrx/store';
import {NmosActions} from "../actions/nmos.actions";
import {normalize} from "../utils/normalizer.utils";

export interface NmosState {
    status: string;
    devices: any;
    deviceIds: any[];
    receivers: any;
    receiverIds: any[]
    senders: any;
    senderIds: any[];
    flows: any;
    flowIds: any[];
    nodes: any;
    nodeIds: any[];
    staged: any[];
    stageMode: string;
    requestedTime: string;
    receiverFilters: any;
    senderFilters: any;
    dragData: {
        isDragging: boolean,
        type: string,
        payload: any
    };
}

export const initialState = {
    status: 'loading',
    devices: undefined,
    deviceIds: [],
    receivers: undefined,
    receiverIds: [],
    senders: undefined,
    senderIds: [],
    flows: undefined,
    flowIds: [],
    nodes: undefined,
    nodeIds: [],
    staged: [],
    stageMode: 'activate_immediate',
    requestedTime: '',
    receiverFilters: {
        showVideo: true,
        showAudio: true,
        showData: true,
        showInUse: true,
        textFilter: ''
    },
    senderFilters: {
        textFilter: ''
    },
    dragData: {
        isDragging: false,
        type: '',
        payload: {}
    }
};

const status = (state: string, action: Action, args: any = {}) => {
    switch (action.type) {
        case NmosActions.GET_NODES_SUCCESS:
        case NmosActions.GET_SENDER_FLOW_SUCCESS:
        case NmosActions.GET_DEVICES_SUCCESS:
        case NmosActions.GET_RECEIVERS_SUCCESS:
        case NmosActions.GET_SENDERS_SUCCESS:
            return (args.state.nodes &&
            args.state.devices &&
            args.state.receivers &&
            args.state.senders &&
            args.state.flows &&
            args.state.nodes)
                ? 'loaded'
                : 'loading';
        case NmosActions.GET_NODES_FAILURE:
        case NmosActions.GET_DEVICES_FAILURE:
        case NmosActions.GET_RECEIVERS_FAILURE:
        case NmosActions.GET_SENDERS_FAILURE:
        case NmosActions.GET_SENDER_FLOW_FAILURE:
            return 'error';
        default:
            return state;
    }
};

const nodes = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case NmosActions.GET_NODES_SUCCESS:
            return normalize(action.payload);
        default:
            return state;
    }
};

const nodeIds = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case NmosActions.GET_NODES_SUCCESS:
            return action.payload.map(nodes => nodes.id);
        default:
            return state;
    }
};

const devices = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case NmosActions.GET_DEVICES_SUCCESS:
            return normalize(action.payload
                .map(device => {
                    device.hasReceivers = device.receivers.length > 0;
                    device.hasSenders = device.senders.length > 0;
                    return device;
                }));
        default:
            return state;
    }
};

const deviceIds = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case NmosActions.GET_DEVICES_SUCCESS:
            return action.payload.map(device => device.id);
        default:
            return state;
    }
};

const receivers = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case NmosActions.GET_RECEIVERS_SUCCESS:
            return normalize(action.payload);
        case NmosActions.GET_RECEIVER_CONSTRAINTS_SUCCESS:
            return Object.assign({}, state, {
                [action.payload.receiver_id]: Object.assign({}, state[action.payload.receiver_id], {
                    constraints: action.payload.constraints
                })
            });
        case NmosActions.WS_UPDATE_RECEIVER_SUBSCRIPTION:
            if (!action.payload.grain.data ||
                action.payload.grain.data.length == 0 ||
                !('post' in action.payload.grain.data[0])) {
                return state;
            }
            let pre = action.payload.grain.data[0].pre;
            let post = action.payload.grain.data[0].post;
            if (!(post.id in state) || pre.subscription.sender_id == post.subscription.sender_id) {
                return state;
            }
            return Object.assign({}, state, {
                [post.id]: Object.assign({}, state[post.id], {
                    subscription: Object.assign({}, state[post.id].subscription, {
                        sender_id: post.subscription.sender_id
                    })
                })
            });
        default:
            return state;
    }
};

const receiverIds = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case NmosActions.GET_RECEIVERS_SUCCESS:
            return action.payload.map(receiver => receiver.id);
        default:
            return state;
    }
};

const senders = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case NmosActions.GET_SENDERS_SUCCESS:
            return normalize(action.payload);
        case NmosActions.SET_MANIFEST:
            return Object.assign({}, state, {
                [action.payload.senderId]: Object.assign({}, state[action.payload.senderId], {
                    manifest_content: action.payload.manifestContent
                })
            });
        case NmosActions.GET_SENDER_CONSTRAINTS_SUCCESS:
            return Object.assign({}, state, {
                [action.payload.sender_id]: Object.assign({}, state[action.payload.sender_id], {
                    constraints: action.payload.constraints
                })
            });
        default:
            return state;
    }
};

const senderIds = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case NmosActions.GET_SENDERS_SUCCESS:
            return action.payload.map(sender => sender.id);
        default:
            return state;
    }
};

const flows = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case NmosActions.GET_SENDER_FLOW_SUCCESS:
            return normalize(action.payload);
        default:
            return state;
    }
};

const flowIds = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case NmosActions.GET_SENDER_FLOW_SUCCESS:
            return action.payload.map(flow => flow.id);
        default:
            return state;
    }
};

const staged = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case NmosActions.STAGE:
            return state.concat([{
                receiverDevice: action.payload.receiverDevice.id,
                receiver: action.payload.receiver.id,
                senderDevice: action.payload.senderDevice.id,
                sender: action.payload.sender.id,
                receiverNode: action.payload.receiverNode.id
            }]);
        case NmosActions.DELETE_STAGE:
            return state.slice(0, action.payload).concat(state.slice(action.payload + 1));
        case NmosActions.CLEAR_STAGED:
            return initialState.staged;
        case NmosActions.TAKE_SUCCESS:
            return initialState.staged;
        default:
            return state;
    }
};


const receiverFilters = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case NmosActions.TOGGLE_VIDEO_FILTER:
            return Object.assign({}, state, {
                showVideo: !state.showVideo
            });
        case NmosActions.TOGGLE_AUDIO_FILTER:
            return Object.assign({}, state, {
                showAudio: !state.showAudio
            });
        case NmosActions.TOGGLE_DATA_FILTER:
            return Object.assign({}, state, {
                showData: !state.showData
            });
        case NmosActions.TOGGLE_IN_USE_FILTER:
            return Object.assign({}, state, {
                showInUse: !state.showInUse
            });
        case NmosActions.TRIGGER_RECEIVER_TEXT_FILTER:
            return Object.assign({}, state, {
                textFilter: action.payload
            });
        default:
            return state;
    }
};


const senderFilters = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case NmosActions.TRIGGER_SENDER_TEXT_FILTER:
            return Object.assign({}, state, {
                textFilter: action.payload
            });
        default:
            return state;
    }
};

const stageMode = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case NmosActions.UPDATE_STAGE_MODE:
            return action.payload;
        default:
            return state;
    }
};

const requestedTime = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case NmosActions.UPDATE_REQUESTED_TIME:
            return action.payload;
        default:
            return state;
    }
};

const dragData = (state: any, action: Action, args: any = {}) => {
    switch (action.type) {
        case NmosActions.SET_DRAG_DATA:
            return {
                isDragging: true,
                type: action.payload.type,
                payload: action.payload.payload
            };
        case NmosActions.CLEAR_DRAG_DATA:
            return {
                isDragging: false,
                type: '',
                payload: {}
            };
        default:
            return state;
    }
};

export const nmosReducer: ActionReducer<NmosState> = (state: NmosState = initialState, action: Action) => {
    switch (action.type) {
        case NmosActions.GET_DEVICES_SUCCESS:
            let loadedDevicesState = Object.assign({}, state, {
                devices: devices(state.devices, action),
                deviceIds: deviceIds(state.deviceIds, action)
            });
            return Object.assign(loadedDevicesState, {
                status: status(state.status, action, {state: loadedDevicesState})
            });
        case NmosActions.GET_RECEIVERS_SUCCESS:
            let loadedReceiversState = Object.assign({}, state, {
                receivers: receivers(state.receivers, action),
                receiverIds: receiverIds(state.receiverIds, action)
            });
            return Object.assign(loadedReceiversState, {
                status: status(state.status, action, {state: loadedReceiversState})
            });
        case NmosActions.GET_SENDERS_SUCCESS:
            let s = senders(state.senders, action);
            let loadedSendersState = Object.assign({}, state, {
                senders: s,
                senderIds: senderIds(state.senderIds, action)
            });
            return Object.assign(loadedSendersState, {
                status: status(state.status, action, {state: loadedSendersState, s: true}),
            });
        case NmosActions.GET_SENDER_FLOW_SUCCESS:
            let loadedFlowsState = Object.assign({}, state, {
                flows: flows(state.flows, action),
                flowIds: flowIds(state.flowIds, action)
            });
            return Object.assign(loadedFlowsState, {
                status: status(state.status, action, {state: loadedFlowsState})
            });
        case NmosActions.GET_NODES_SUCCESS:
            let loadedNodesState = Object.assign({}, state, {
                nodes: nodes(state.nodes, action),
                nodeIds: nodeIds(state.nodeIds, action)
            });
            return Object.assign(loadedNodesState, {
                status: status(state.status, action, {state: loadedNodesState})
            });
        case NmosActions.GET_RECEIVER_CONSTRAINTS_SUCCESS:
            return Object.assign({}, state, {
                receivers: receivers(state.receivers, action)
            });
        case NmosActions.GET_SENDER_CONSTRAINTS_SUCCESS:
            return Object.assign({}, state, {
                senders: senders(state.senders, action)
            });
        case NmosActions.STAGE:
            return Object.assign({}, state, {
                staged: staged(state.staged, action),
            });
        case NmosActions.DELETE_STAGE:
            return Object.assign({}, state, {
                staged: staged(state.staged, action),
            });
        case NmosActions.CLEAR_STAGED:
            return Object.assign({}, state, {
                staged: staged(state.staged, action),
            });
        case NmosActions.UPDATE_STAGE_MODE:
            return Object.assign({}, state, {
                stageMode: stageMode(state.stageMode, action),
            });
        case NmosActions.UPDATE_REQUESTED_TIME:
            return Object.assign({}, state, {
                requestedTime: requestedTime(state.requestedTime, action),
            });
        case NmosActions.TAKE_SUCCESS:
            return Object.assign({}, state, {
                staged: staged(state.staged, action),
            });
        case NmosActions.SET_MANIFEST:
            return Object.assign({}, state, {
                senders: senders(state.senders, action),
            });
        case NmosActions.SET_DRAG_DATA:
            return Object.assign({}, state, {
                dragData: dragData(state.dragData, action),
            });
        case NmosActions.CLEAR_DRAG_DATA:
            return Object.assign({}, state, {
                dragData: dragData(state.dragData, action),
            });
        case NmosActions.TOGGLE_VIDEO_FILTER:
            return Object.assign({}, state, {
                receiverFilters: receiverFilters(state.receiverFilters, action),
            });
        case NmosActions.TOGGLE_AUDIO_FILTER:
            return Object.assign({}, state, {
                receiverFilters: receiverFilters(state.receiverFilters, action),
            });
        case NmosActions.TOGGLE_DATA_FILTER:
            return Object.assign({}, state, {
                receiverFilters: receiverFilters(state.receiverFilters, action),
            });
        case NmosActions.TOGGLE_IN_USE_FILTER:
            return Object.assign({}, state, {
                receiverFilters: receiverFilters(state.receiverFilters, action),
            });
        case NmosActions.TRIGGER_RECEIVER_TEXT_FILTER:
            return Object.assign({}, state, {
                receiverFilters: receiverFilters(state.receiverFilters, action),
            });
        case NmosActions.TRIGGER_SENDER_TEXT_FILTER:
            return Object.assign({}, state, {
                senderFilters: senderFilters(state.senderFilters, action),
            });
        case NmosActions.WS_UPDATE_RECEIVER_SUBSCRIPTION:
            return Object.assign({}, state, {
                receivers: receivers(state.receivers, action)
            });
        default:
            return state;
    }
};
