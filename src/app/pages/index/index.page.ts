import {Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../reducers/index';
import {NmosActions} from "../../actions/nmos.actions";
import {NmosSelectors} from "../../selectors/nmos.selectors";

@Component({
    selector: 'index-page',
    templateUrl: './index.page.html',
    styleUrls: ['./index.page.css']
})
export class IndexPage {
    status: any;
    receiverDevices: any;
    senderDevices: any;
    staged: any;
    stageMode: any;
    receiverFilters: any;
    senderFilters: any;
    requestedTime: any;
    dragData: any;

    constructor(private store: Store<AppState>,
                private nmosActions: NmosActions,
                private nmosSelectors: NmosSelectors) {
        this.store.dispatch(this.nmosActions.subscribeReceivers());
        this.store.dispatch(this.nmosActions.getNodes());
        this.store.dispatch(this.nmosActions.getDevices());
        this.store.dispatch(this.nmosActions.getReceivers());
        this.store.dispatch(this.nmosActions.getSenders());
        this.store.dispatch(this.nmosActions.getSenderFlow());

        this.status = this.nmosSelectors.getStatus();
        this.receiverDevices = this.nmosSelectors.getReceiverDevices();
        this.senderDevices = this.nmosSelectors.getSenderDevices();
        this.staged = this.nmosSelectors.getStaged();
        this.stageMode = this.nmosSelectors.getStageMode();
        this.receiverFilters = this.nmosSelectors.getReceiverFilters();
        this.senderFilters = this.nmosSelectors.getSenderFilters();
        this.requestedTime = this.nmosSelectors.getRequestedTime();
        this.dragData = this.nmosSelectors.getDragData();
    }

    stage(data) {
        this.store.dispatch(this.nmosActions.stage(data));
    }

    deleteStage(data) {
        this.store.dispatch(this.nmosActions.deleteStage(data));
    }

    clearStaged(data) {
        this.store.dispatch(this.nmosActions.clearStaged());
    }

    take(data) {
        this.store.dispatch(this.nmosActions.take(data));
    }

    abort(data) {
        this.store.dispatch(this.nmosActions.abort(data));
    }

    free(data) {
        this.store.dispatch(this.nmosActions.free(data));
    }

    bulkFree(data) {
        this.store.dispatch(this.nmosActions.bulkFree(data));
    }

    setDragData(data) {
        this.store.dispatch(this.nmosActions.setDragData(data));
    }

    clearDragData(data) {
        this.store.dispatch(this.nmosActions.clearDragData(data));
    }

    updateStageMode(data) {
        this.store.dispatch(this.nmosActions.updateStageMode(data));
    }

    updateRequestedTime(data) {
        this.store.dispatch(this.nmosActions.updateRequestedTime(data));
    }

    toggleVideoFilter(data) {
        this.store.dispatch(this.nmosActions.toggleVideoFilter(data));
    }

    toggleAudioFilter(data) {
        this.store.dispatch(this.nmosActions.toggleAudioFilter(data));
    }

    toggleDataFilter(data) {
        this.store.dispatch(this.nmosActions.toggleDataFilter(data));
    }

    toggleInUseFilter(data) {
        this.store.dispatch(this.nmosActions.toggleInUseFilter(data));
    }

    triggerReceiverTextFilter(data) {
        this.store.dispatch(this.nmosActions.triggerReceiverTextFilter(data));
    }

    triggerSenderTextFilter(data) {
        this.store.dispatch(this.nmosActions.triggerSenderTextFilter(data));
    }
}
