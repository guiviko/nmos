import {FormActions} from './form.actions';
import {NmosActions} from "./nmos.actions";

export {
    FormActions,
    NmosActions
};

export default [
    FormActions,
    NmosActions
];