import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';

@Injectable()
export class NmosActions {
    static GET_DEVICES = 'NMOS_GET_DEVICES';
    getDevices(): Action {
        return {
            type: NmosActions.GET_DEVICES
        };
    }

    static GET_DEVICES_SUCCESS = 'NMOS_GET_DEVICES_SUCCESS';
    getDevicesSuccess(data): Action {
        return {
            type: NmosActions.GET_DEVICES_SUCCESS,
            payload: data
        };
    }

    static GET_DEVICES_FAILURE = 'NMOS_GET_DEVICES_FAILURE';
    getDevicesFailure(): Action {
        return {
            type: NmosActions.GET_DEVICES_FAILURE
        };
    }

    static GET_RECEIVERS = 'NMOS_GET_RECEIVERS';
    getReceivers(): Action {
        return {
            type: NmosActions.GET_RECEIVERS
        };
    }

    static GET_RECEIVERS_SUCCESS = 'NMOS_GET_RECEIVERS_SUCCESS';
    getReceiversSuccess(data): Action {
        return {
            type: NmosActions.GET_RECEIVERS_SUCCESS,
            payload: data
        };
    }

    static GET_RECEIVERS_FAILURE = 'NMOS_GET_RECEIVERS_FAILURE';
    getReceiversFailure(): Action {
        return {
            type: NmosActions.GET_RECEIVERS_FAILURE
        };
    }

    static GET_SENDERS = 'NMOS_GET_SENDERS';
    getSenders(): Action {
        return {
            type: NmosActions.GET_SENDERS
        };
    }

    static GET_SENDERS_SUCCESS = 'NMOS_GET_SENDERS_SUCCESS';
    getSendersSuccess(data): Action {
        return {
            type: NmosActions.GET_SENDERS_SUCCESS,
            payload: data
        };
    }

    static GET_SENDERS_FAILURE = 'NMOS_GET_SENDERS_FAILURE';
    getSendersFailure(): Action {
        return {
            type: NmosActions.GET_SENDERS_FAILURE
        };
    }

    static GET_SENDER_FLOW = 'NMOS_GET_SENDER_FLOW';
    getSenderFlow(): Action {
        return {
            type: NmosActions.GET_SENDER_FLOW
        };
    }

    static GET_SENDER_FLOW_SUCCESS = 'NMOS_GET_SENDER_FLOW_SUCCESS';
    getSenderFlowSuccess(data): Action {
        return {
            type: NmosActions.GET_SENDER_FLOW_SUCCESS,
            payload: data
        };
    }

    static GET_SENDER_FLOW_FAILURE = 'NMOS_GET_SENDER_FLOW_FAILURE';
    getSenderFlowFailure(): Action {
        return {
            type: NmosActions.GET_SENDER_FLOW_FAILURE
        };
    }

    static GET_NODES = 'NMOS_GET_NODES';
    getNodes(): Action {
        return {
            type: NmosActions.GET_NODES
        };
    }

    static GET_NODES_SUCCESS = 'NMOS_GET_NODES_SUCCESS';
    getNodesSuccess(data): Action {
        return {
            type: NmosActions.GET_NODES_SUCCESS,
            payload: data
        };
    }

    static GET_NODES_FAILURE = 'NMOS_GET_NODES_FAILURE';
    getNodesFailure(): Action {
        return {
            type: NmosActions.GET_NODES_FAILURE
        };
    }

    static GET_RECEIVER_CONSTRAINTS = 'NMOS_GET_RECEIVER_CONSTRAINTS';
    getReceiverConstraints(data): Action {
        return {
            type: NmosActions.GET_RECEIVER_CONSTRAINTS,
            payload: data
        };
    }

    static GET_RECEIVER_CONSTRAINTS_SUCCESS = 'NMOS_GET_RECEIVER_CONSTRAINTS_SUCCESS';
    getReceiverConstraintsSuccess(data): Action {
        return {
            type: NmosActions.GET_RECEIVER_CONSTRAINTS_SUCCESS,
            payload: data
        };
    }

    static GET_RECEIVER_CONSTRAINTS_FAILURE = 'NMOS_GET_RECEIVER_CONSTRAINTS_FAILURE';
    getReceiverConstraintsFailure(data): Action {
        return {
            type: NmosActions.GET_RECEIVER_CONSTRAINTS_FAILURE,
            payload: data
        };
    }

    static GET_SENDER_CONSTRAINTS = 'NMOS_GET_SENDER_CONSTRAINTS';
    getSenderConstraints(data): Action {
        return {
            type: NmosActions.GET_SENDER_CONSTRAINTS,
            payload: data
        };
    }

    static GET_SENDER_CONSTRAINTS_SUCCESS = 'NMOS_GET_SENDER_CONSTRAINTS_SUCCESS';
    getSenderConstraintsSuccess(data): Action {
        return {
            type: NmosActions.GET_SENDER_CONSTRAINTS_SUCCESS,
            payload: data
        };
    }

    static GET_SENDER_CONSTRAINTS_FAILURE = 'NMOS_GET_SENDER_CONSTRAINTS_FAILURE';
    getSenderConstraintsFailure(data): Action {
        return {
            type: NmosActions.GET_SENDER_CONSTRAINTS_FAILURE,
            payload: data
        };
    }

    static SUBSCRIBE_RECEIVERS = 'NMOS_SUBSCRIBE_RECEIVERS';
    subscribeReceivers(): Action {
        return {
            type: NmosActions.SUBSCRIBE_RECEIVERS
        };
    }

    static SUBSCRIBE_RECEIVERS_SUCCESS = 'NMOS_SUBSCRIBE_RECEIVERS_SUCCESS';
    subscribeReceiversSuccess(data): Action {
        return {
            type: NmosActions.SUBSCRIBE_RECEIVERS_SUCCESS,
            payload: data
        };
    }

    static SUBSCRIBE_RECEIVERS_FAILURE = 'NMOS_SUBSCRIBE_RECEIVERS_FAILURE';
    subscribeReceiversFailure(): Action {
        return {
            type: NmosActions.SUBSCRIBE_RECEIVERS_FAILURE
        };
    }

    static STAGE = 'NMOS_STAGE';
    stage(data): Action {
        return {
            type: NmosActions.STAGE,
            payload: data
        };
    }

    static DELETE_STAGE = 'NMOS_DELETE_STAGE';
    deleteStage(data): Action {
        return {
            type: NmosActions.DELETE_STAGE,
            payload: data
        };
    }

    static TAKE = 'NMOS_TAKE';
    take(data): Action {
        return {
            type: NmosActions.TAKE,
            payload: data
        };
    }

    static TAKE_SUCCESS = 'NMOS_TAKE_SUCCESS';
    takeSuccess(data): Action {
        return {
            type: NmosActions.TAKE_SUCCESS,
            payload: data
        };
    }

    static TAKE_FAILURE = 'NMOS_TAKE_FAILURE';
    takeFailure(): Action {
        return {
            type: NmosActions.TAKE_FAILURE
        };
    }

    static SET_MANIFEST = 'NMOS_SET_MANIFEST';
    setManifest(data): Action {
        return {
            type: NmosActions.SET_MANIFEST,
            payload: data
        };
    }

    static CLEAR_STAGED = 'NMOS_CLEAR_STAGED';
    clearStaged(): Action {
        return {
            type: NmosActions.CLEAR_STAGED
        };
    }

    static ABORT = 'NMOS_ABORT';
    abort(data): Action {
        return {
            type: NmosActions.ABORT,
            payload: data
        };
    }

    static ABORT_SUCCESS = 'NMOS_ABORT_SUCCESS';
    abortSuccess(data): Action {
        return {
            type: NmosActions.ABORT_SUCCESS,
            payload: data
        };
    }

    static ABORT_FAILURE = 'NMOS_ABORT_FAILURE';
    abortFailure(data): Action {
        return {
            type: NmosActions.ABORT_FAILURE,
            payload: data
        };
    }

    static FREE = 'NMOS_FREE';
    free(data): Action {
        return {
            type: NmosActions.FREE,
            payload: data
        };
    }

    static FREE_SUCCESS = 'NMOS_FREE_SUCCESS';
    freeSuccess(data): Action {
        return {
            type: NmosActions.FREE_SUCCESS,
            payload: data
        };
    }

    static FREE_FAILURE = 'NMOS_FREE_FAILURE';
    freeFailure(data): Action {
        return {
            type: NmosActions.FREE_FAILURE,
            payload: data
        };
    }

    static BULK_FREE = 'NMOS_BULK_FREE';
    bulkFree(data): Action {
        return {
            type: NmosActions.BULK_FREE,
            payload: data
        };
    }

    static BULK_FREE_SUCCESS = 'NMOS_BULK_FREE_SUCCESS';
    bulkFreeSuccess(data): Action {
        return {
            type: NmosActions.BULK_FREE_SUCCESS,
            payload: data
        };
    }

    static BULK_FREE_FAILURE = 'NMOS_BULK_FREE_FAILURE';
    bulkFreeFailure(data): Action {
        return {
            type: NmosActions.BULK_FREE_FAILURE,
            payload: data
        };
    }

    static SET_DRAG_DATA = 'NMOS_SET_DRAG_DATA';
    setDragData(data): Action {
        return {
            type: NmosActions.SET_DRAG_DATA,
            payload: data
        };
    }

    static CLEAR_DRAG_DATA = 'NMOS_CLEAR_DRAG_DATA';
    clearDragData(data): Action {
        return {
            type: NmosActions.CLEAR_DRAG_DATA
        };
    }

    static UPDATE_STAGE_MODE = 'NMOS_UPDATE_STAGE_MODE';
    updateStageMode(data): Action {
        return {
            type: NmosActions.UPDATE_STAGE_MODE,
            payload: data
        };
    }

    static UPDATE_REQUESTED_TIME = 'NMOS_UPDATE_REQUESTED_TIME';
    updateRequestedTime(data): Action {
        return {
            type: NmosActions.UPDATE_REQUESTED_TIME,
            payload: data
        };
    }

    static TOGGLE_VIDEO_FILTER = 'NMOS_TOGGLE_VIDEO_FILTER';
    toggleVideoFilter(data): Action {
        return {
            type: NmosActions.TOGGLE_VIDEO_FILTER,
            payload: data
        };
    }

    static TOGGLE_AUDIO_FILTER = 'NMOS_TOGGLE_AUDIO_FILTER';
    toggleAudioFilter(data): Action {
        return {
            type: NmosActions.TOGGLE_AUDIO_FILTER,
            payload: data
        };
    }

    static TOGGLE_DATA_FILTER = 'NMOS_TOGGLE_DATA_FILTER';
    toggleDataFilter(data): Action {
        return {
            type: NmosActions.TOGGLE_DATA_FILTER,
            payload: data
        };
    }

    static TOGGLE_IN_USE_FILTER = 'NMOS_TOGGLE_IN_USE_FILTER';
    toggleInUseFilter(data): Action {
        return {
            type: NmosActions.TOGGLE_IN_USE_FILTER,
            payload: data
        };
    }

    static TRIGGER_RECEIVER_TEXT_FILTER = 'NMOS_TRIGGER_RECEIVER_TEXT_FILTER';
    triggerReceiverTextFilter(data): Action {
        return {
            type: NmosActions.TRIGGER_RECEIVER_TEXT_FILTER,
            payload: data
        };
    }

    static TRIGGER_SENDER_TEXT_FILTER = 'NMOS_TRIGGER_SENDER_TEXT_FILTER';
    triggerSenderTextFilter(data): Action {
        return {
            type: NmosActions.TRIGGER_SENDER_TEXT_FILTER,
            payload: data
        };
    }

    static WS_UPDATE_RECEIVER_SUBSCRIPTION = 'NMOS_WS_UPDATE_RECEIVER_SUBSCRIPTION';
    wsUpdateReceiverSubscription(data): Action {
        return {
            type: NmosActions.WS_UPDATE_RECEIVER_SUBSCRIPTION,
            payload: data
        };
    }
}
