import { IndexPage } from './pages/index/index.page';
import {MainHeader} from './components/mainHeader/mainHeader';
import {MainMenu} from './components/mainMenu/mainMenu';
import { NotFound404Component } from './not-found404.component';
import {ReceiverList} from "./components/receiverList/receiverList";
import {SenderList} from "./components/senderList/senderList";
import {StagingList} from "./components/stagingList/stagingList";
import {PyrDraggable} from "./components/generic/pyrDraggable/pyrDraggable.directive";
import {PyrDroppable} from "./components/generic/pyrDraggable/pyrDroppable.directive";
import {DevicesStatus} from "./components/devicesStatus/devicesStatus";

export const APP_DECLARATIONS = [
    IndexPage,
    MainHeader,
    MainMenu,
    ReceiverList,
    SenderList,
    StagingList,
    PyrDraggable,
    PyrDroppable,
    DevicesStatus,
    NotFound404Component,
];
