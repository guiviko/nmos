import {Injectable} from '@angular/core';
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";

import {AppState} from "../reducers/index";

@Injectable()
export class NmosSelectors {
    constructor(private store: Store<AppState>) {
    }

    getStatus(): Observable<any> {
        return this.store.select(s => s.nmos.status);
    }

    getReceiverDevices(): Observable<any> {
        return this.store.select(s => {
            if (s.nmos.status != 'loaded') {
                return Observable.from([]);
            }

            return s.nmos.deviceIds
                .map(deviceId => s.nmos.devices[deviceId])
                .filter(device => device.hasReceivers)
                .filter(device => s.nmos.receiverFilters.textFilter == '' ||
                device.label.toUpperCase().indexOf(s.nmos.receiverFilters.textFilter.toUpperCase()) >= 0)
                .map(device => this.buildDevice(device, s))
                .filter(device => device.receivers.length > 0);
        });
    }

    getSenderDevices(): Observable<any> {
        return this.store.select(s => {
            if (s.nmos.status != 'loaded') {
                return Observable.from([]);
            }
            return s.nmos.deviceIds
                .map(deviceId => s.nmos.devices[deviceId])
                .filter(device => device.hasSenders)
                .filter(device => s.nmos.senderFilters.textFilter == '' ||
                device.label.toUpperCase().indexOf(s.nmos.senderFilters.textFilter.toUpperCase()) >= 0)
                .map(device => this.buildDevice(device, s))
                .filter(device => device.senders.length > 0);
        });
    }

    buildDevice(device, s) {
        let copy = Object.assign({}, device);

        copy.receivers = device.receivers.map(receiverId => s.nmos.receivers[receiverId] ? s.nmos.receivers[receiverId] : receiverId)
            .map(receiver => {
                if (!receiver.label) {
                    return receiver;
                }
                let parts = receiver.format.split(':');
                let type = parts[parts.length - 1];
                return Object.assign(receiver, {
                    receiverType: type == 'data' ? 'ANC' : type.toUpperCase()
                })
            })
            .filter(receiver => (receiver.receiverType == 'VIDEO' && s.nmos.receiverFilters.showVideo) ||
            (receiver.receiverType == 'AUDIO' && s.nmos.receiverFilters.showAudio) ||
            (receiver.receiverType == 'ANC' && s.nmos.receiverFilters.showData))
            .filter(receiver => s.nmos.receiverFilters.showInUse || !receiver.subscription.sender_id);
        copy.senders = device.senders.map(senderId => {
            if (s.nmos.senders[senderId]) {
                let sender = s.nmos.senders[senderId];
                sender.flow = s.nmos.flows[sender.flow_id] ? s.nmos.flows[sender.flow_id] : undefined;
                if (sender.flow) {
                    let type = sender.flow.media_type.substr(0, 3).toUpperCase();
                    switch (type) {
                        case 'VID':
                            sender.flow.type = 'VIDEO';
                            break;
                        case 'AUD':
                            sender.flow.type = 'AUDIO';
                            break;
                        case 'ANC':
                            sender.flow.type = 'ANC';
                            break;
                    }
                }
                return sender;
            } else {
                return undefined;
            }
        });
        copy.node = s.nmos.nodes[device.node_id];

        return copy;
    }

    getStaged() {
        return this.store.select(s => s.nmos.staged.map(stage => Object.assign({}, stage, {
            receiverDevice: s.nmos.devices[stage.receiverDevice],
            receiver: s.nmos.receivers[stage.receiver],
            senderDevice: s.nmos.devices[stage.senderDevice],
            sender: s.nmos.senders[stage.sender],
            receiverNode: s.nmos.nodes[stage.receiverNode]
        })));
    }

    getStageMode() {
        return this.store.select(s => s.nmos.stageMode);
    }

    getRequestedTime() {
        return this.store.select(s => s.nmos.requestedTime);
    }

    getDragData() {
        return this.store.select(s => s.nmos.dragData);
    }

    getReceiverFilters() {
        return this.store.select(s => s.nmos.receiverFilters);
    }

    getSenderFilters() {
        return this.store.select(s => s.nmos.senderFilters);
    }
}
