import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import {Observable} from "rxjs";
import {NmosActions} from "../actions/nmos.actions";
import {NmosService} from "../services/nmos.service";
import {AppState} from "../reducers/index";
import {Store} from "@ngrx/store";

@Injectable()
export class NmosEffects {
    constructor(private actions$: Actions,
                private store$: Store<AppState>,
                private nmosActions: NmosActions,
                private nmosService: NmosService)
    { }

    @Effect() subscribeReceivers = this.actions$
        .ofType(NmosActions.SUBSCRIBE_RECEIVERS)
        .mergeMap(action => {
            return this.nmosService.subscribeReceivers()
                .map(data => this.nmosActions.subscribeReceiversSuccess(data))
                .catch((err) => Observable.of(this.nmosActions.subscribeReceiversFailure()))
        });

    @Effect({dispatch: false}) subscribeReceiversSucces = this.actions$
        .ofType(NmosActions.SUBSCRIBE_RECEIVERS_SUCCESS)
        .do(action => {
            this.nmosService.openSocketReceivers(action.payload.ws_href)
                .subscribe(response => {
                    this.store$.dispatch(this.nmosActions.wsUpdateReceiverSubscription(JSON.parse(response.data)));
                });
        });

    @Effect() getDevices = this.actions$
        .ofType(NmosActions.GET_DEVICES)
        .mergeMap(action => {
            return this.nmosService.getDevices()
                .map(data => this.nmosActions.getDevicesSuccess(data))
                .catch((err) => Observable.of(this.nmosActions.getDevicesFailure()))
        });

    @Effect() getNodes = this.actions$
        .ofType(NmosActions.GET_NODES)
        .mergeMap(action => {
            return this.nmosService.getNodes()
                .map(data => this.nmosActions.getNodesSuccess(data))
                .catch((err) => Observable.of(this.nmosActions.getNodesFailure()))
        });

    @Effect() getReceivers = this.actions$
        .ofType(NmosActions.GET_RECEIVERS)
        .mergeMap(action => {
            return this.nmosService.getReceivers()
                .map(data => this.nmosActions.getReceiversSuccess(data))
                .catch((err) => Observable.of(this.nmosActions.getReceiversFailure()))
        });

    @Effect() getSenders = this.actions$
        .ofType(NmosActions.GET_SENDERS)
        .mergeMap(action => {
            return this.nmosService.getSenders()
                .map(data => this.nmosActions.getSendersSuccess(data))
                .catch((err) => Observable.of(this.nmosActions.getSendersFailure()))
        });

    @Effect() getFlows = this.actions$
        .ofType(NmosActions.GET_SENDER_FLOW)
        .mergeMap(action => {
            return this.nmosService.getSenderFlow()
                .map(data => this.nmosActions.getSenderFlowSuccess(data))
                .catch((err) => Observable.of(this.nmosActions.getSenderFlowFailure()))
        });

    // @Effect() getReceiversSuccess = this.actions$
    //     .ofType(NmosActions.GET_RECEIVERS_SUCCESS)
    //     .withLatestFrom(this.store$)
    //     .mergeMap(([action, state]) => {
    //         return action.payload.map(receiver => this.nmosActions.getReceiverConstraints({
    //                 receiverId: receiver.id,
    //                 href: 'some'
    //             }));
    //     });

    // @Effect() getSendersSuccess = this.actions$
    //     .ofType(NmosActions.GET_SENDERS_SUCCESS)
    //     .withLatestFrom(this.store$)
    //     .mergeMap(([action, state]) => {
    //         return action.payload.map(sender => this.nmosActions.getSenderFlow(sender))
    //             .concat(action.payload.map(sender => this.nmosActions.getSenderConstraints({
    //                 senderId: sender.id,
    //                 href: state.nmos.nodes[state.nmos.devices[sender.device_id].node_id].href
    //             })));
    //     });

    // @Effect() getReceiverConstraints = this.actions$
    //     .ofType(NmosActions.GET_RECEIVER_CONSTRAINTS)
    //     .mergeMap(action => {
    //         return this.nmosService.getReceiverConstraints(action.payload)
    //             .map(data => this.nmosActions.getReceiverConstraintsSuccess({constraints: data, receiver_id: action.payload.id}))
    //             .catch((err) => Observable.of(this.nmosActions.getReceiverConstraintsFailure(err)))
    //     });
    //
    // @Effect() getSenderConstraints = this.actions$
    //     .ofType(NmosActions.GET_SENDER_CONSTRAINTS)
    //     .mergeMap(action => {
    //         return this.nmosService.getSenderConstraints(action.payload)
    //             .map(data => this.nmosActions.getSenderConstraintsSuccess({constraints: data, sender_id: action.payload.id}))
    //             .catch((err) => Observable.of(this.nmosActions.getSenderConstraintsFailure(err)))
    //     });

    @Effect() take = this.actions$
        .ofType(NmosActions.TAKE)
        .mergeMap(action => {
            let requestData = {
                baseAddress: action.payload.staged[0].receiverNode.href,
                payload: []
            };

            action.payload.staged.map(stage => {
                requestData.payload.push({
                    id: stage.receiver.id,
                    params: {
                        sender_id: stage.sender.id,
                        master_enable: true,
                        activation: {
                            mode: action.payload.mode,
                            requested_time: action.payload.mode == 'activate_scheduled_relative' || action.payload.mode == 'activate_scheduled_absolute' ? action.payload.requestedTime : null
                        },
                        transport_file: {
                            data: stage.sender.manifest_content,
                            type: 'application/sdp'
                        }
                    }
                });
            });

            return this.nmosService.take(requestData)
                .map(data => this.nmosActions.takeSuccess(data))
                .catch((err) => Observable.of(this.nmosActions.takeFailure()))
        });

    @Effect() abort = this.actions$
        .ofType(NmosActions.ABORT)
        .mergeMap(action => {
            let requestData = {
                baseAddress: action.payload.receiverNode.href,
                receiverId: action.payload.receiver.id,
                payload: {
                    master_enable: true,
                    activation: {
                        mode: null,
                        requested_time: null
                    }
                }
            };

            return this.nmosService.abort(requestData)
                .map(data => this.nmosActions.abortSuccess(data))
                .catch((err) => Observable.of(this.nmosActions.abortFailure(err)))
        });

    @Effect() free = this.actions$
        .ofType(NmosActions.FREE)
        .mergeMap(action => {
            let requestData = {
                baseAddress: action.payload.receiverNode.href,
                receiverId: action.payload.receiver.id,
                payload: {
                    master_enable: false,
                    activation: {
                        mode: null,
                        requested_time: null
                    }
                }
            };

            return this.nmosService.free(requestData)
                .map(data => this.nmosActions.freeSuccess(data))
                .catch((err) => Observable.of(this.nmosActions.freeFailure(err)))
        });

    @Effect() bulkFree = this.actions$
        .ofType(NmosActions.BULK_FREE)
        .mergeMap(action => {
            let requestData = {
                baseAddress: action.payload.node.href,
                payload: []
            };

            action.payload.receivers.map(receiver => {
                requestData.payload.push({
                    id: receiver.id,
                    params: {
                        master_enable: false,
                        activation: {
                            mode: null,
                            requested_time: null
                        }
                    }
                });
            });

            return this.nmosService.bulkFree(requestData)
                .map(data => this.nmosActions.bulkFreeSuccess(data))
                .catch((err) => Observable.of(this.nmosActions.bulkFreeFailure(err)))
        });
}