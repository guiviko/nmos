import {Directive, ElementRef, HostListener, Input, EventEmitter, Output} from '@angular/core';
import {PyrDraggableService} from "../../../services/generic/pyrDraggable.service";

@Directive({
    selector: '[pyrDraggable]'
})
export class PyrDraggable {
    /**
     * The id of the drag event
     */
    @Input() dragType: string;

    /**
     * The drag event's payload
     */
    @Input() dragPayload: any;

    /**
     * The mode to operate in
     * -> normal: drag and drop without manipulating original elements
     */
    @Input() mode: string;

    /**
     * on drag start event
     */
    @Output() dragStart = new EventEmitter<any>();

    /**
     * on drag end event
     */
    @Output() dragEnd = new EventEmitter<any>();

    /**
     * The native element reference
     */
    el;

    constructor(private elRef: ElementRef,
                private pyrDraggableService: PyrDraggableService) {
        this.el = elRef.nativeElement;
    }

    @HostListener('mousedown', ['$event']) handleMouseDown(e) {
        e.preventDefault();

        this.pyrDraggableService.handleMouseDown(this.el, this.dragType, this.dragPayload, this.mode, e.clientX,
            e.clientY, this.dragStart, this.dragEnd);
    }
}