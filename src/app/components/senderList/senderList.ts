import {Component, Input, Output, EventEmitter, ChangeDetectionStrategy} from '@angular/core';

@Component({
    selector: 'sender-list',
    templateUrl: './senderList.html',
    styleUrls: ['./senderList.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SenderList {
    @Input() status: any;
    @Input() devices: any;
    @Output() setDragData = new EventEmitter<any>();
    @Output() clearDragData = new EventEmitter<any>();
    @Output() triggerSenderTextFilter = new EventEmitter<any>();
}