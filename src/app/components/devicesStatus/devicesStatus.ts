import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
    selector: 'devices-status',
    templateUrl: './devicesStatus.html',
    styleUrls: ['./devicesStatus.css']
})
export class DevicesStatus {
    @Input() status;
    @Input() loadingText;
    @Input() errorText;
    @Input() noDataText;
}