import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
    selector: 'receiver-list',
    templateUrl: './receiverList.html',
    styleUrls: ['./receiverList.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReceiverList {
    @Input() status: any;
    @Input() devices: any;
    @Input() dragData: any;
    @Input() filters: any;
    @Output() stage = new EventEmitter<any>();
    @Output() abort = new EventEmitter<any>();
    @Output() free = new EventEmitter<any>();
    @Output() bulkFree = new EventEmitter<any>();
    @Output() toggleVideoFilter = new EventEmitter<any>();
    @Output() toggleAudioFilter = new EventEmitter<any>();
    @Output() toggleDataFilter = new EventEmitter<any>();
    @Output() toggleInUseFilter = new EventEmitter<any>();
    @Output() triggerReceiverTextFilter = new EventEmitter<any>();

    handleDrop(data, receiverDevice, receiver, receiverNode) {
        switch (data.type.split('-')[0]) {
            case 'sender':
                this.handleSenderDrop(data.payload, receiverDevice, receiver, receiverNode);
                break;
            case 'abort':
                this.handleAbortDrop(data.payload, receiver, receiverNode);
                break;
            case 'free':
                this.handleFreeDrop(data.payload, receiver, receiverNode);
                break;
        }
    }

    handleHeaderDrop(data, device) {
        this.bulkFree.emit(device);
    }

    handleDeviceDrop(data, device) {
        data = data.payload;
        data.senders.map(sender => {
            switch (sender.flow.type) {
                case 'VIDEO':
                    let videoReceivers = device.receivers.filter(receiver => receiver.receiverType == 'VIDEO');
                    if (videoReceivers.length > 0) {
                        this.stage.emit({
                            receiverDevice: device,
                            receiver: videoReceivers[0],
                            senderDevice: data,
                            sender: sender,
                            receiverNode: device.node
                        });
                    }
                    break;
                case 'ANC':
                    let dataReceivers = device.receivers.filter(receiver => receiver.receiverType == 'ANC');
                    if (dataReceivers.length > 0) {
                        this.stage.emit({
                            receiverDevice: device,
                            receiver: dataReceivers[0],
                            senderDevice: data,
                            sender: sender,
                            receiverNode: device.node
                        });
                    }
                    break;
            }
        });

        let audioSenders = data.senders.filter(sender => sender.flow.type == 'AUDIO');
        let audioReceivers = device.receivers.filter(receiver => receiver.receiverType == 'AUDIO');
        for (let i = 0; i < audioSenders.length; i++) {
            if (audioReceivers.length >= i + 1) {
                this.stage.emit({
                    receiverDevice: device,
                    receiver: audioReceivers[i],
                    senderDevice: data,
                    sender: audioSenders[i],
                    receiverNode: device.node
                });
            }
        }
    }

    handleSenderDrop(data, receiverDevice, receiver, receiverNode) {
        this.stage.emit({
            receiverDevice: receiverDevice,
            receiver: receiver,
            senderDevice: data.senderDevice,
            sender: data.sender,
            receiverNode: receiverNode
        });
    }

    handleAbortDrop(data, receiver, receiverNode) {
        this.abort.emit({
            receiver: receiver,
            receiverNode: receiverNode
        });
    }

    handleFreeDrop(data, receiver, receiverNode) {
        this.free.emit({
            receiver: receiver,
            receiverNode: receiverNode
        });
    }
}