import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from "@angular/core";

@Component({
    selector: 'staging-list',
    templateUrl: './stagingList.html',
    styleUrls: ['./stagingList.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class StagingList {
    @Input() staged;
    @Input() stageMode;
    @Input() requestedTime;
    @Output() deleteStage = new EventEmitter<any>();
    @Output() take = new EventEmitter<any>();
    @Output() clearStaged = new EventEmitter<any>();
    @Output() updateStageMode = new EventEmitter<any>();
    @Output() updateRequestedTime = new EventEmitter<any>();
}