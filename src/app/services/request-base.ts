import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class RequestBase extends RequestOptions {
  constructor() {
    super({
      headers: new Headers({
        'Content-Type': 'application/json',
      })
    });
  }
}
