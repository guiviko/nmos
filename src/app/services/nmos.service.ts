import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import {API_BASE_URL} from "./constants";
import {AppState} from "../reducers/index";
import {Store} from "@ngrx/store";
import {NmosActions} from "../actions/nmos.actions";
import * as Rx from 'rxjs/Rx';

@Injectable()
export class NmosService {
    registryIp: string;
    queryUriPrefix: string;
    connectionUriPrefix: string;
    jsonHeaderOptions: RequestOptions;
    sdpHeaderOptions: RequestOptions;

    constructor(private http: Http,
                private store: Store<AppState>,
                private nmosActions: NmosActions) {
        this.registryIp = '10.37.65.20:8235';
        this.queryUriPrefix = 'x-nmos/query/v1.2';
        this.connectionUriPrefix = 'x-nmos/connection/v1.0';
        let headers = new Headers({
            'Accept': 'application/json'
        });
        this.jsonHeaderOptions = new RequestOptions({headers: headers});
        let sdpHeaders = new Headers({
            'Accept': 'application/*'
        });
        this.sdpHeaderOptions = new RequestOptions({headers: sdpHeaders});
    }

    create(url) {
        let ws = new WebSocket(url);

        let observable = Rx.Observable.create((obs: Rx.Observer<MessageEvent>) => {
            ws.onmessage = obs.next.bind(obs);
            ws.onerror = obs.error.bind(obs);
            ws.onclose = obs.complete.bind(obs);

            return ws.close.bind(ws);
        });

        let observer = {
            next: (data: Object) => {
                if (ws.readyState === WebSocket.OPEN) {
                    ws.send(JSON.stringify(data));
                }
            },
            complete: () => {
                console.log('Web socket closed.');
            }
        };

        return Rx.Subject.create(observer, observable);
    }

    getNodes() {
        return this.http.get('http://' + this.registryIp + '/' + this.queryUriPrefix + '/nodes/', this.jsonHeaderOptions)
            .map(response => response.json());
    }

    subscribeReceivers(): any {
        return this.http.post('http://' + this.registryIp + '/' + this.queryUriPrefix + '/subscriptions', {
            max_update_rate_ms: 1000,
            params: {},
            persist: false,
            resource_path: '/receivers'
        }, this.jsonHeaderOptions)
            .map(response => response.json());
    }

    openSocketReceivers(ws_href): any {
        let wsArgument = ws_href.substr(ws_href.indexOf('/ws'));
        return this.create('ws://' + this.registryIp + wsArgument);
    }

    getDevices(): any {
        return this.http.get('http://' + this.registryIp + '/' + this.queryUriPrefix + '/devices/', this.jsonHeaderOptions)
            .map(response => response.json());
    }

    getReceivers(): any {
        return this.http.get('http://' + this.registryIp + '/' + this.queryUriPrefix + '/receivers/', this.jsonHeaderOptions)
            .map(response => response.json());
    }

    getSenders() {
        return this.http.get('http://' + this.registryIp + '/' + this.queryUriPrefix + '/senders/', this.jsonHeaderOptions)
            .map(response => response.json())
            .map(senders => {
                senders.map(sender => {
                    this.http.get(sender.manifest_href, this.sdpHeaderOptions)
                        .subscribe(response => {
                            this.store.dispatch(this.nmosActions.setManifest({
                                senderId: sender.id,
                                manifestContent: response.text()
                            }));
                        });
                });
                return senders;
            });
    }

    getSenderFlow() {
        return this.http.get('http://' + this.registryIp + '/' + this.queryUriPrefix + '/flows/', this.jsonHeaderOptions)
            .map(response => response.json());
    }

    getReceiverConstraints(data) {
        return this.http.post(API_BASE_URL + '/api/nmos-connection-get-request', {
            url: data.href + 'x-nmos/connection/v1.2/single/receivers/' + data.receiverId + '/constraints'
        })
            .map(response => response.json());
    }

    getSenderConstraints(data) {
        return this.http.post(API_BASE_URL + '/api/nmos-connection-get-request', {
            url: data.href + 'x-nmos/connection/v1.2/single/senders/' + data.senderId + '/constraints'
    })
            .map(response => response.json());
    }

    take(data) {
        return this.http.post(data.baseAddress + this.connectionUriPrefix + '/bulk/receivers', data.payload, this.jsonHeaderOptions)
            .map(response => response.json());
    }

    abort(data) {
        return this.http.patch(data.baseAddress + this.connectionUriPrefix + '/single/receivers/' + data.receiverId + '/staged', data.payload, this.jsonHeaderOptions)
            .map(response => response.json());
    }

    free(data) {
        return this.http.patch(data.baseAddress + this.connectionUriPrefix + '/single/receivers/' + data.receiverId + '/staged', data.payload, this.jsonHeaderOptions)
            .map(response => response.json());
    }

    bulkFree(data) {
        return this.http.post(data.baseAddress + this.connectionUriPrefix + '/bulk/receivers', data.payload, this.jsonHeaderOptions)
            .map(response => response.json());
    }
}
