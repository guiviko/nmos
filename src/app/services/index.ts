import {NmosService} from "./nmos.service";
import {PyrDraggableService} from "./generic/pyrDraggable.service";

export default [
    PyrDraggableService,
    NmosService
];