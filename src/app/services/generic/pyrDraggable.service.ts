import {Injectable} from '@angular/core';

@Injectable()
export class PyrDraggableService {
    /**
     * When dragging is fully initialized.
     */
    isDragging: boolean;

    /**
     * When element is clicked, but mouse hasn't moved
     * enough to start a drag.
     */
    isPreDragging: boolean;

    /**
     * The drag type.
     */
    dragType: string;

    /**
     * The drag payload to be passed.
     */
    dragPayload: any;

    /**
     * Initial mouse x position on drag start
     */
    initialMouseX;

    /**
     * Initial mouse y position on drag start
     */
    initialMouseY;

    /**
     * Initial element left position on drag start
     */
    initialLeft;

    /**
     * Initial element top position on drag start
     */
    initialTop;

    /**
     * The element reference.
     */
    el;

    /**
     * The event emitter for drag start.
     */
    startDragEmitter;

    /**
     * The event emitter for drag end.
     */
    endDragEmitter;

    /**
     * The mode to operate in
     * -> normal: drag and drop without manipulating original elements
     */
    mode: string;

    /**
     * The element clone that will be dragged.
     */
    clone;

    constructor() {
        this.stopDrag();

        window.addEventListener('mousemove', e => this.handleMouseMove(e));
        window.addEventListener('mouseup', e => this.handleMouseUp(e));
    }

    handleMouseUp(e) {
        if (this.isPreDragging) {
            this.isPreDragging = false;
            return;
        }

        if (!this.isDragging) {
            return;
        }

        this.startDragEmitter.emit({});

        if ((!this.mode || this.mode == 'normal') && this.el.parentElement && this.clone) {
            this.el.parentElement.removeChild(this.clone);
        }

        this.stopDrag();
    }

    handleMouseMove(e) {
        if (this.isPreDragging) {
            if (Math.abs(e.clientX - this.initialMouseX) > 2 || Math.abs(e.clientY - this.initialMouseY) > 2) {
                this.isPreDragging = false;
                this.startDragging();
            }
            return;
        }

        if (!this.isDragging) {
            return;
        }

        // this is to disable text selection when dragging
        e.preventDefault();

        this.clone.style.left = this.initialLeft + (e.clientX - this.initialMouseX) + 'px';
        this.clone.style.top = this.initialTop + (e.clientY - this.initialMouseY) + 'px';
    }

    startDragging() {
        this.startDragEmitter.emit({});

        if (!this.mode || this.mode == 'normal') {
            this.clone = this.el.cloneNode(true);
            this.el.classList.add('pyr-is-dragged');
        }

        let boundingRect = this.el.getBoundingClientRect();

        this.initialLeft = boundingRect.left;
        this.initialTop = boundingRect.top;

        this.clone.style.position = 'absolute';
        this.clone.style.width = boundingRect.width + 'px';
        this.clone.style.height = boundingRect.height + 'px';
        this.clone.style.left = this.initialLeft + 'px';
        this.clone.style.top = this.initialTop + 'px';
        this.clone.style.pointerEvents = 'none';
        this.clone.style.boxShadow = '0px 0px 5px 0px rgba(0,0,0,0.75)';
        this.clone.style.zIndex = 1000;

        if (!this.mode || this.mode == 'normal') {
            this.el.parentElement.insertBefore(this.clone, this.el);
        }

        this.isDragging = true;
    }

    handleMouseDown(el: any, type: string, payload: any, mode: string, initialMouseX: number, initialMouseY: number, startDragEmitter: any, endDragEmitter: any) {
        this.el = el;
        this.dragType = type;
        this.dragPayload = payload;
        this.mode = mode;
        this.initialMouseX = initialMouseX;
        this.initialMouseY = initialMouseY;
        this.startDragEmitter = startDragEmitter;
        this.endDragEmitter = endDragEmitter;
        this.isPreDragging = true;
    }

    stopDrag() {
        this.el = {};
        this.dragType = '';
        this.dragPayload = {};
        this.mode = '';
        this.initialMouseX = 0;
        this.initialMouseY = 0;
        this.startDragEmitter = {};
        this.endDragEmitter = {};
        this.isDragging = false;
    }
}