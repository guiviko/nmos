<?php

namespace App\Http\Controllers\Api;

use App\Domain\Services\NmosSimulator;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\NmosRequest;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Library\Http\Response;

class NmosController extends Controller
{
    private const SIMULATE = false;

    public function nmosRequest(NmosRequest $request, NmosSimulator $simulatorService)
    {
        if (self::SIMULATE)
        {
            return $simulatorService->nmosRequest($request);
        }

        $guzzleClient = new Client([
            'verify' => false
        ]);

        try
        {
            $response = $guzzleClient->get($request->data('url'), [
                'connect_timeout' => 3.0
            ]);

            return new Response($response->getStatusCode(), json_decode($response->getBody(), true));
        }
        catch (RequestException $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not send nmos request: '.$e->getMessage());
        }
    }

    public function nmosManifestRequest(NmosRequest $request)
    {
        $guzzleClient = new Client([
            'verify' => false
        ]);

        try
        {
            $response = $guzzleClient->get($request->data('url'), [
                'connect_timeout' => 3.0
            ]);

            return new Response($response->getStatusCode(), $response->getBody(), false);
        }
        catch (RequestException $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not send nmos manifest request: '.$e->getMessage());
        }
    }

    public function nmosTakeRequest(NmosRequest $request)
    {
        $guzzleClient = new Client([
            'verify' => false
        ]);

        try
        {
            $response = $guzzleClient->post($request->data('url'), [
                'connect_timeout' => 3.0,
                'json' => $request->data('payload'),
                'headers'  => ['content-type' => 'application/json', 'Accept' => 'application/json']
            ]);

            return new Response($response->getStatusCode(), $response->getBody(), false);
        }
        catch (RequestException $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not send nmos take request: '.$e->getMessage());
        }
    }

    public function nmosAbortRequest(NmosRequest $request)
    {
        $guzzleClient = new Client([
            'verify' => false
        ]);

        try
        {
            $response = $guzzleClient->patch($request->data('url'), [
                'connect_timeout' => 3.0,
                'json' => $request->data('payload'),
                'headers'  => ['content-type' => 'application/json', 'Accept' => 'application/json']
            ]);

            return new Response($response->getStatusCode(), json_decode($response->getBody(), true));
        }
        catch (RequestException $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not send nmos abort request: '.$e->getMessage());
        }
    }

    public function nmosFreeRequest(NmosRequest $request)
    {
        $guzzleClient = new Client([
            'verify' => false
        ]);

        try
        {
            $response = $guzzleClient->patch($request->data('url'), [
                'connect_timeout' => 3.0,
                'json' => $request->data('payload'),
                'headers'  => ['content-type' => 'application/json', 'Accept' => 'application/json']
            ]);

            return new Response($response->getStatusCode(), json_decode($response->getBody(), true));
        }
        catch (RequestException $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not send nmos free request: '.$e->getMessage());
        }
    }

    public function nmosConnectionGetRequest(NmosRequest $request)
    {
        $guzzleClient = new Client([
            'verify' => false
        ]);

        try
        {
            $response = $guzzleClient->get($request->data('url'), [
                'connect_timeout' => 3.0
            ]);

            return new Response($response->getStatusCode(), json_decode($response->getBody(), true));
        }
        catch (RequestException $e)
        {
            return new Response(Response::STATUS_BAD_REQUEST, 'Could not send nmos connection request: '.$e->getMessage());
        }
    }
}